import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
        <header class="header-app">
            <div class="container">
                <h1>Instruct</h1>
                <input type="text" id="filter" placeholder="Busca o domínio do e-mail" />
            </div>
        </header>
        <main>
            <h2>
                teste
            </h2>
        </main>
        <footer>
          <div class="container">
              <span class="left">
                  Roberto Borges&copy;2020
	    			</span>
              <span class="right">
                  <a href="https://robertoborges.com.br" target="_blank">robertoborges.com.br</a>
              </span>
          </div>
        </footer>
    </div>
  );
}

export default App;
